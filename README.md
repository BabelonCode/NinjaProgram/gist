```
using System;
using System.Collections.Generic;

namespace Challenges
{
    public class Measurement
    {
        public DateTime Date { get; set; }
        public MeasurementData Data { get; set; }
    }

    public class MeasurementData
    {
        public IEnumerable<MeasurementItem> MorningMeasurements { get; set; }
        public IEnumerable<MeasurementItem> EveningMeasurements { get; set; }
    }

    public class MeasurementItem
    {
        public decimal Temperature { get; set; }
        public decimal Pressure { get; set; }

        public override string ToString()
        {
            return $"T={Temperature} P={Pressure}";
        }
    }

    public class MeasurementService
    {
        private int accessCount = 0;

        public IEnumerable<Measurement> GetMeasurements()
        {
            yield return new Measurement
            {
                Date = DateTime.UtcNow,
                Data = new MeasurementData
                {
                    MorningMeasurements = GetMorningMeasurements(10),
                    EveningMeasurements = GetEveningMeasurements(10)
                }
            };
        }

        public int GetAccessNumber()
        {
            return accessCount;
        }

        private IEnumerable<MeasurementItem> GetEveningMeasurements(int count)
        {
            Random random = new Random();
            while ((count--) > 0)
            {
                ++accessCount;

                yield return new MeasurementItem
                {
                    Temperature = -random.Next(),
                    Pressure = random.Next()
                };
            }
        }

        private IEnumerable<MeasurementItem> GetMorningMeasurements(int count)
        {
            Random random = new Random();
            while ((count--) > 0)
            {
                ++accessCount;

                yield return new MeasurementItem
                {
                    Temperature = random.Next(),
                    Pressure = random.Next()
                };
            }
        }
    }

    public class WeatherCast
    {
        private MeasurementService _measurementService;

        public WeatherCast()
        {
            _measurementService = new MeasurementService();
        }

        public void Process()
        {
            var measurements = _measurementService.GetMeasurements();
            Print(measurements);
            Save(measurements);
            Publish(measurements);
            Print(measurements);

            Console.WriteLine(_measurementService.GetAccessNumber());
        }

        private void Publish(IEnumerable<Measurement> measurements)
        {
            foreach (var measurement in measurements)
            {
                foreach (var item in measurement.Data.MorningMeasurements)
                {
                    PublishMorning(item);
                }

                foreach (var item in measurement.Data.EveningMeasurements)
                {
                    PublishEvening(item);
                }
            }
        }

        private void Print(IEnumerable<Measurement> measurements)
        {
            foreach (var measurement in measurements)
            {
                foreach (var item in measurement.Data.MorningMeasurements)
                {
                    PrintMorning(item);
                }
                
                foreach (var item in measurement.Data.EveningMeasurements)
                {
                    PrintEvening(item);
                }
            }
        }

        private void Save(IEnumerable<Measurement> measurements)
        {
            foreach (var measurement in measurements)
            {
                foreach (var item in measurement.Data.MorningMeasurements)
                {
                    SaveMorning(item);
                }

                foreach (var item in measurement.Data.EveningMeasurements)
                {
                    SaveEvening(item);
                }
            }
        }

        private void PublishEvening(MeasurementItem item)
        { }

        private void PublishMorning(MeasurementItem item)
        { }

        private void SaveEvening(MeasurementItem item)
        { }

        private void SaveMorning(MeasurementItem item)
        { }

        private void PrintMorning(MeasurementItem item)
        {
            Console.WriteLine("Morning " + item);
        }

        private void PrintEvening(MeasurementItem item)
        {
            Console.WriteLine("Evening " + item);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            WeatherCast weatherCast = new WeatherCast();
            weatherCast.Process();
        }
    }
}
```
